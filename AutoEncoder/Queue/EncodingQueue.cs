﻿using AutoEncoder.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoEncoder.Queue
{
    public class EncodingQueue
    {

        private static List<EncodeTask> todo = new List<EncodeTask>();
        public static bool AlreadyProcessing = false;

        public static void ProcessQueue()
        {

            if (!AlreadyProcessing)
            {

                foreach (EncodeTask task in todo)
                {

                    AlreadyProcessing = true;

                    if (!task.Done)
                    {

                        Console.Title = $"Encoding {task.FileName}...";

                        Log.Information($"Encoding " + task.FileName);

                        string args = "";

                        if (Encoder.config.HWAcceleration)
                            args += "-hwaccel ";

                        args += $"cuvid -i \"{task.E.FullPath}\" -c:v h264_nvenc -pix_fmt yuv420p -preset slow -rc vbr_hq -b:vc 8M -maxrate:v 10M -c:a aac -b:a 224k \"{Encoder.config.EncodeFolderPath}\\{task.E.Name}.mp4\"";

                        ProcessStartInfo startInfo = new ProcessStartInfo();

                        startInfo.UseShellExecute = true;
#if !DEBUG
                        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
#else
                        startInfo.WindowStyle = ProcessWindowStyle.Normal;
#endif

                        if (Encoder.isFFMpegLocallyDownloaded)
                        {

                            FileInfo info = new FileInfo("ffmpeg.exe");
                            startInfo.FileName = info.FullName;

                        }
                        else
                        {

                            startInfo.FileName = "ffmpeg.exe";

                        }

                        startInfo.Arguments = args;

                        Process proc = Process.Start(startInfo);
                        proc.WaitForExit();

                        while (!proc.HasExited)
                        {

                            continue;

                        }

                        File.Delete(task.E.FullPath);

                        task.Done = true;

                    }

                }

                Console.Title = "AutoEncoder";

                if (todo.Count != 0)
                    ClearQueue();

                AlreadyProcessing = false;
                Log.Information("The encoding queue is now empty.");

            }

        }

        public static int GetQueueLength()
        {

            return todo.Count;

        }

        public static void AddToQueue(EncodeTask task)
        {

            todo.Add(task);

        }

        public static void RemoveFromQueue(EncodeTask task)
        {

            todo.Remove(task);

        }

        public static void ClearQueue()
        {

            todo.Clear();

        }

    }
}
