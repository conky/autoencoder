﻿using AutoEncoder.Models;
using AutoEncoder.Queue;
using AutoEncoder.Utils;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace AutoEncoder
{
    public class Encoder
    {

        public static Config config;
        public static bool isFFMpegLocallyDownloaded = false;

        static void Main(string[] args) => new Encoder().Start().GetAwaiter().GetResult();

        private async Task Start()
        {

            Console.Title = "AutoEncoder";

            Console.WriteLine("Initialising logger");
            Log.Logger = new LoggerConfiguration().WriteTo.Async(a => a.Console()).CreateLogger();

            Log.Information("Logger initialised");

            Log.Information("Hooking exception handlers");

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Log.Information("Exception handlers hooked");

            Log.Information("Reading from config");

            Configuration.CreateConfigIfNotExists();
            config = Configuration.ReadConfig();

            Log.Information($"Config loaded: " + config.ToString());
           
            Log.Information("Checking for ffmpeg");

            if (Processes.IsFFMpegInstalled())
            {

                Log.Information("ffmpeg was detected");

            } else
            {

                Log.Error("ffmpeg was not detected, installing");
                isFFMpegLocallyDownloaded = true;
                Processes.DownloadFFMpeg();


            }

            Log.Information("Initialising file system watcher");

            try
            {

                FileSystemWatcher watcher = new FileSystemWatcher();
                watcher.Filter = "*.*";
                watcher.IncludeSubdirectories = false;
                watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.CreationTime | NotifyFilters.FileName | NotifyFilters.Size;
                watcher.Path = config.WatchFolderPath;

                watcher.Created += Watcher_Created;

                watcher.EnableRaisingEvents = true;

            } catch (Exception e)
            {

                Log.Fatal("An exception has occurred (have you specified an invalid path?): " + e.Message);
                Console.ReadKey();
                Environment.Exit(-1);

            }

            Log.Information("Initialised file system watcher");

            Log.Information("Creating queue processor");
            Timer timer = new Timer();

            timer.Interval = config.CheckInterval;
            timer.Elapsed += Timer_Elapsed;

            timer.Start();

            Log.Information("Queue processor created");

            await Task.Delay(-1);

        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {

            Log.Error($"An exception has occurred: {e}");

        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {

            if (!EncodingQueue.AlreadyProcessing && EncodingQueue.GetQueueLength() != 0)
                EncodingQueue.ProcessQueue();

        }

        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {

            FileInfo fileInfo = new FileInfo(e.FullPath);

            while (FileUtils.IsFileLocked(e.FullPath))
            {

                continue;

            }

            EncodingQueue.AddToQueue(new EncodeTask { E = e, FileName = e.Name, FilePath = e.FullPath });
            Log.Information($"Added {e.Name} to the encoding queue");

        }
    }
}
