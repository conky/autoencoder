﻿using AutoEncoder.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoEncoder.Utils
{
    public class Configuration
    {

        public static void CreateConfigIfNotExists()
        {

            if (!File.Exists("config.json"))
            {
               
                dynamic json = new JObject();

                json.checkinterval = 30000;
                json.outputpath = "encoded";
                json.watchfolder = "raw";
                json.hardwareacceleration = true;

                File.WriteAllText("config.json", json.ToString());

            }

        }

        public static Config ReadConfig()
        {

            string line, config = "";

            using (FileStream f = new FileStream("config.json", FileMode.Open))
            {

                using (StreamReader r = new StreamReader(f))
                {

                    while ((line = r.ReadLine()) != null) {

                        config += line;

                    }

                    r.Close();
                    f.Close();

                }

            }

            JObject json = JObject.Parse(config);

            return new Config { CheckInterval = int.Parse(json["checkinterval"].ToString()), EncodeFolderPath = json["outputpath"].ToString(), HWAcceleration = bool.Parse(json["hardwareacceleration"].ToString()), WatchFolderPath = json["watchfolder"].ToString() };

        }

    }
}
