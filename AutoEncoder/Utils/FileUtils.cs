﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AutoEncoder.Utils
{
    public class FileUtils
    {
        
        public static bool IsFileLocked(string filePath)
        {

            try
            {

                using (File.Open(filePath, FileMode.Open)) { }

            } catch (IOException e)
            {

                var errorCode = Marshal.GetHRForException(e) & ((1 << 16) - 1);
                return errorCode == 32 || errorCode == 33;

            }

            return false;

        }

    }
}
