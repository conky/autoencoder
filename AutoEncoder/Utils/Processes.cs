﻿using Serilog;
using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;

namespace AutoEncoder.Utils
{
    public class Processes
    {
       
        public static void DownloadFFMpeg()
        {

            using (WebClient client = new WebClient())
            {

                client.DownloadFileCompleted += Client_DownloadFileCompleted;
                client.DownloadProgressChanged += Client_DownloadProgressChanged;

                client.DownloadFileAsync(new Uri("https://ffmpeg.zeranoe.com/builds/win64/static/ffmpeg-4.0.2-win64-static.zip"), "ffmpeg.zip");

            }

        }

        private static int previousPercentage = 0;

        private static void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {

            if (previousPercentage != e.ProgressPercentage) // only print if progress has changed
            {
                Log.Information($"Downloading ffmpeg: {e.ProgressPercentage}%, {NetworkUtils.Format(e.BytesReceived)} / {NetworkUtils.Format(e.TotalBytesToReceive)}");
                previousPercentage = e.ProgressPercentage;
            }

        }

        private static void Client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {

            Log.Information("Download complete, extracting ffmpeg.exe");

            using (ZipArchive zip = ZipFile.OpenRead("ffmpeg.zip"))
            {

                foreach(ZipArchiveEntry entry in zip.Entries)
                {

                    if (entry.FullName.EndsWith(".exe", StringComparison.OrdinalIgnoreCase))
                    {

                        if (entry.Name.Equals("ffmpeg.exe"))
                        {

                            entry.ExtractToFile("ffmpeg.exe");
                            Log.Information("ffmpeg.exe has been successfully extracted");
                            break;

                        }

                    }

                }

            }

            File.Delete("ffmpeg.zip");

        }

        public static bool IsFFMpegInstalled()
        {
            
            try
            {
                Process proc = Process.Start("ffmpeg");
                proc.WaitForExit();

                return true;

            } catch (Exception )
            {

                if (!File.Exists("ffmpeg.exe"))
                    return false;
                else
                    return true;

            } 

        }

    }
}
