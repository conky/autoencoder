﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoEncoder.Utils
{
    public class NetworkUtils
    {

        private static string[] UNITS = { "B", "KB", "MB", "GB" };

        public static string Format(double bytes)
        {
            
            int i = 0;
            do
            {

                bytes /= 1024;
                i++;

            } while (bytes > 1024);

            return $"{Math.Round(bytes, 2)} {UNITS[i]}";

        }

    }
}
