﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoEncoder.Models
{
    public class Config
    {

        public int CheckInterval { get; set; }
        public string WatchFolderPath { get; set; }
        public string EncodeFolderPath { get; set; }
        public bool HWAcceleration { get; set; }

        public override string ToString()
        {

            return $"<Check Interval: {CheckInterval}ms, Hardware Acceleration: {HWAcceleration}, Watch Folder Path: {WatchFolderPath}, Encoded File Output Path: {EncodeFolderPath}>";

        }

    }
}
