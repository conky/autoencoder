﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoEncoder.Models
{
    public class EncodeTask
    {

        public string FilePath { get; set; }
        public string FileName { get; set; }
        public FileSystemEventArgs E { get; set; }
        public bool Done { get; set; }

    }
}
